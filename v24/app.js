const app = Vue.createApp({
    data() {
        return {
            boxAselected:false,
            boxBselected:false,
            boxCselected:false,
        };
    },
    methods:{
        borderbox(item)
        {
          if('A' === item){
              this.boxAselected = !this.boxAselected;
              this.boxBselected = false;
              this.boxCselected = false;
          }else if('B' === item){
              this.boxBselected = !this.boxBselected;
              this.boxAselected = false;
              this.boxCselected = false;
          }else if('C' === item){
              this.boxCselected = !this.boxCselected;
              this.boxBselected = false;
              this.boxAselected = false;
          } 
        },

    },

});

app.mount('#styling')