const app = Vue.createApp({
    data(){
      return {
        counter:0,
        name:'',
        lname:'',
        full_name:'',
        cname:'',
      };
    },
    watch:{
      name(value){
        console.log('watch fname');
        this.full_name = value + ' ' + this.lname;
      },
      lname(value){
        console.log('watch lname');
        this.full_name = this.name + ' ' + value;
      },
    },

    computed: {
      fname(){
        console.log('computed fname');
        return this.name + ' ' + this.lname;
      },
      counteritem(){
        console.log('computed counteritem');
        return this.counter;
      },
    },

    methods: {
      fullname(){
        console.log('fullname');
        return this.name + ' ' + this.lname;
      },
      resetInput(){
        console.log('resetinput');
        this.name = '';
        this.lname = '';
        this.confirmname();
      },
      confirmname(){
        console.log('confirm');
        this.cname = this.name + ' ' + this.lname;
      },
      submitform(event){
        console.log('submit');
        // event.preventDefault();
        alert('submit ok');
      },
      setName(event,fname){
        console.log('setname');
        this.name =  event.target.value ;
        if(this.name ==='')
        {
          this.name = '';
        }
        else 
        {
          this.name = fname + ' ' + event.target.value + ' ' + 'Faruk';
        }
        
      },
      add(item){
        console.log('add');
        this.counter =  this.counter + item;
      },
      remove(item){
        console.log('remove');
        if(this.counter == 0){
          this.counter = 0;
        }
        else {
          this.counter = this.counter -  item;
        }
        
      },
    },
});

app.mount('#events');