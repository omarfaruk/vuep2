const app = Vue.createApp({
    data() {
        return {
            para:'This is data binding',
            link:'https://onlineshop.faruk-bd.xyz',
            coursea:'Finish vue js versions 2',
            courseb:'Start vue js version 3',
        }
    },
    methods: {
        randomfunc(){
            const randnumber = Math.random();
            if(randnumber < 0.5)
            {
                return this.coursea;
            }
            else {
                return this.courseb;
            }
        },
    },
});

app.mount('#app')